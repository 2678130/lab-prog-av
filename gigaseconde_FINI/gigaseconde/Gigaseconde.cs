using System;

public static class Gigaseconde
{
    /*
        Vous recevez un moment, déterminez le moment après y avoir ajouté une gigaseconde.
        Une gigaseconde correspond à 10 ^ 9(1 000 000 000) secondes.
     */

    public static DateTime Ajouter(DateTime moment)
    {
        return moment.AddSeconds(1000000000);
    }
}