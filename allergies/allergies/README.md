# Les allergies

Compte tenu du score d'allergie d'une personne, veuillez déterminer si elle est allergique ou non à un élément donné. De plus, vous devez déterminer la liste complète de ses allergies.

Un test d'allergie produit un score numérique unique qui contient les informations sur toutes les allergies de la personne.

La liste des allergènes (et leur valeur) qui ont été testés est: (vous devez usiliser le enum Allergene)

- oeufs (1)
- arachides (2)
- crustacés (4)
- fraises (8)
- tomates (16)
- chocolat (32)
- pollen (64)
- chats (128)

Donc, si Robert est allergique aux arachides et au chocolat, il obtient un score de 34.

Étant donné un score de 34, votre programme devrait pouvoir déterminer:

- Si Robert est allergique à l'un des allergènes énumérés ci-dessus.
- Tous les allergènes auxquels Robert est allergique.

## Notes de mise en œuvre

Un score donné peut inclure des allergènes ** non ** énumérés ci-dessus (c.-à-d. allergènes ayant comme valeur 256, 512, 1024, etc.). Votre programme devrait ignorer ces composants de la partition. Par exemple, si le score d'allergie est de 257, votre programme ne doit signaler que l'allergie aux œufs (1).

## Astuces

Cet exercice demande que vous connaissiez les [opérateurs logiques booléens](https://docs.microsoft.com/fr-ca/dotnet/csharp/language-reference/operators/boolean-logical-operators).

## Exécution des tests

Pour exécuter les tests, exécutez la commande `dotnet test` à partir du répertoire de l'exercice.

Initialement, seul le premier test sera activé. C'est pour vous encourager à résoudre l'exercice une étape à la fois.
Une fois que vous avez réussi le premier test, supprimez la propriété `Skip` du test suivant et travaillez à faire passer ce prochain test.
Une fois qu'aucun des tests n'est ignoré et qu'ils réussissent tous, vous devez soumettre votre solution.
