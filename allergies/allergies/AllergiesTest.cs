using Xunit;

public class AllergiesTest
{
    [Fact]
    public void Testing_pour_oeufs_pas_allergique_a_rien()
    {
        var sut = new Allergies(0);
        Assert.False(sut.EstAllergique(Allergene.Oeufs));
    }

    [Fact]
    public void Testing_pour_oeufs_allergique_juste_aux_oeufs()
    {
        var sut = new Allergies(1);
        Assert.True(sut.EstAllergique(Allergene.Oeufs));
    }

    [Fact]
    public void Testing_pour_oeufs_allergique_aux_oeufs_et_quelquechose_autre()
    {
        var sut = new Allergies(3);
        Assert.True(sut.EstAllergique(Allergene.Oeufs));
    }

    [Fact]
    public void Testing_pour_oeufs_allergique_a_quelquechose_mais_pas_aux_oeufs()
    {
        var sut = new Allergies(2);
        Assert.False(sut.EstAllergique(Allergene.Oeufs));
    }

    [Fact]
    public void Testing_pour_oeufs_allergique_a_tout()
    {
        var sut = new Allergies(255);
        Assert.True(sut.EstAllergique(Allergene.Oeufs));
    }

    [Fact]
    public void Testing_pour_arachides_pas_allergique_a_rien()
    {
        var sut = new Allergies(0);
        Assert.False(sut.EstAllergique(Allergene.Arachides));
    }

    [Fact]
    public void Testing_pour_arachides_allergique_juste_aux_arachides()
    {
        var sut = new Allergies(2);
        Assert.True(sut.EstAllergique(Allergene.Arachides));
    }

    [Fact]
    public void Testing_pour_arachides_allergique_aux_arachides_et_quelquechose_autre()
    {
        var sut = new Allergies(7);
        Assert.True(sut.EstAllergique(Allergene.Arachides));
    }

    [Fact]
    public void Testing_pour_arachides_allergique_a_quelquechose_mais_pas_aux_arachides()
    {
        var sut = new Allergies(5);
        Assert.False(sut.EstAllergique(Allergene.Arachides));
    }

    [Fact]
    public void Testing_pour_arachides_allergique_a_tout()
    {
        var sut = new Allergies(255);
        Assert.True(sut.EstAllergique(Allergene.Arachides));
    }

    [Fact]
    public void Testing_pour_crustaces_pas_allergique_a_rien()
    {
        var sut = new Allergies(0);
        Assert.False(sut.EstAllergique(Allergene.Crustaces));
    }

    [Fact]
    public void Testing_pour_crustaces_allergique_juste_aux_crustaces()
    {
        var sut = new Allergies(4);
        Assert.True(sut.EstAllergique(Allergene.Crustaces));
    }

    [Fact]
    public void Testing_pour_crustaces_allergique_aux_crustaces_et_quelquechose_autre()
    {
        var sut = new Allergies(14);
        Assert.True(sut.EstAllergique(Allergene.Crustaces));
    }

    [Fact]
    public void Testing_pour_crustaces_allergique_a_quelquechose_mais_pas_aux_crustaces()
    {
        var sut = new Allergies(10);
        Assert.False(sut.EstAllergique(Allergene.Crustaces));
    }

    [Fact]
    public void Testing_pour_crustaces_allergique_a_tout()
    {
        var sut = new Allergies(255);
        Assert.True(sut.EstAllergique(Allergene.Crustaces));
    }

    [Fact]
    public void Testing_pour_fraises_pas_allergique_a_rien()
    {
        var sut = new Allergies(0);
        Assert.False(sut.EstAllergique(Allergene.Fraises));
    }

    [Fact]
    public void Testing_pour_fraises_allergique_juste_aux_fraises()
    {
        var sut = new Allergies(8);
        Assert.True(sut.EstAllergique(Allergene.Fraises));
    }

    [Fact]
    public void Testing_pour_fraises_allergique_aux_fraises_et_quelquechose_autre()
    {
        var sut = new Allergies(28);
        Assert.True(sut.EstAllergique(Allergene.Fraises));
    }

    [Fact]
    public void Testing_pour_fraises_allergique_a_quelquechose_mais_pas_aux_fraises()
    {
        var sut = new Allergies(20);
        Assert.False(sut.EstAllergique(Allergene.Fraises));
    }

    [Fact]
    public void Testing_pour_fraises_allergique_a_tout()
    {
        var sut = new Allergies(255);
        Assert.True(sut.EstAllergique(Allergene.Fraises));
    }

    [Fact]
    public void Testing_pour_tomates_pas_allergique_a_rien()
    {
        var sut = new Allergies(0);
        Assert.False(sut.EstAllergique(Allergene.Tomates));
    }

    [Fact]
    public void Testing_pour_tomates_allergique_juste_aux_tomates()
    {
        var sut = new Allergies(16);
        Assert.True(sut.EstAllergique(Allergene.Tomates));
    }

    [Fact]
    public void Testing_pour_tomates_allergique_aux_tomates_et_quelquechose_autre()
    {
        var sut = new Allergies(56);
        Assert.True(sut.EstAllergique(Allergene.Tomates));
    }

    [Fact]
    public void Testing_pour_tomates_allergique_a_quelquechose_mais_pas_aux_tomates()
    {
        var sut = new Allergies(40);
        Assert.False(sut.EstAllergique(Allergene.Tomates));
    }

    [Fact]
    public void Testing_pour_tomates_allergique_a_tout()
    {
        var sut = new Allergies(255);
        Assert.True(sut.EstAllergique(Allergene.Tomates));
    }

    [Fact]
    public void Testing_pour_chocolat_pas_allergique_a_rien()
    {
        var sut = new Allergies(0);
        Assert.False(sut.EstAllergique(Allergene.Chocolat));
    }

    [Fact]
    public void Testing_pour_chocolat_allergique_juste_au_chocolat()
    {
        var sut = new Allergies(32);
        Assert.True(sut.EstAllergique(Allergene.Chocolat));
    }

    [Fact]
    public void Testing_pour_chocolat_allergique_au_chocolat_et_quelquechose_autre()
    {
        var sut = new Allergies(112);
        Assert.True(sut.EstAllergique(Allergene.Chocolat));
    }

    [Fact]
    public void Testing_pour_chocolat_allergique_a_quelquechose_mais_pas_au_chocolat()
    {
        var sut = new Allergies(80);
        Assert.False(sut.EstAllergique(Allergene.Chocolat));
    }

    [Fact]
    public void Testing_pour_chocolat_allergique_a_tout()
    {
        var sut = new Allergies(255);
        Assert.True(sut.EstAllergique(Allergene.Chocolat));
    }

    [Fact]
    public void Testing_pour_pollen_pas_allergique_a_rien()
    {
        var sut = new Allergies(0);
        Assert.False(sut.EstAllergique(Allergene.Pollen));
    }

    [Fact]
    public void Testing_pour_pollen_allergique_juste_au_pollen()
    {
        var sut = new Allergies(64);
        Assert.True(sut.EstAllergique(Allergene.Pollen));
    }

    [Fact]
    public void Testing_pour_pollen_allergique_au_pollen_et_quelquechose_autre()
    {
        var sut = new Allergies(224);
        Assert.True(sut.EstAllergique(Allergene.Pollen));
    }

    [Fact]
    public void Testing_pour_pollen_allergique_a_quelquechose_mais_pas_au_pollen()
    {
        var sut = new Allergies(160);
        Assert.False(sut.EstAllergique(Allergene.Pollen));
    }

    [Fact]
    public void Testing_pour_pollen_allergique_a_tout()
    {
        var sut = new Allergies(255);
        Assert.True(sut.EstAllergique(Allergene.Pollen));
    }

    [Fact]
    public void Testing_pour_chats_pas_allergique_a_rien()
    {
        var sut = new Allergies(0);
        Assert.False(sut.EstAllergique(Allergene.Chats));
    }

    [Fact]
    public void Testing_pour_chats_allergique_juste_aux_chats()
    {
        var sut = new Allergies(128);
        Assert.True(sut.EstAllergique(Allergene.Chats));
    }

    [Fact]
    public void Testing_pour_chats_allergique_aux_chats_et_quelquechose_autre()
    {
        var sut = new Allergies(192);
        Assert.True(sut.EstAllergique(Allergene.Chats));
    }

    [Fact]
    public void Testing_pour_chats_allergique_a_quelquechose_mais_pas_aux_chats()
    {
        var sut = new Allergies(64);
        Assert.False(sut.EstAllergique(Allergene.Chats));
    }

    [Fact]
    public void Testing_pour_chats_allergique_a_tout()
    {
        var sut = new Allergies(255);
        Assert.True(sut.EstAllergique(Allergene.Chats));
    }

    [Fact]
    public void A_rien()
    {
        var sut = new Allergies(0);
        Assert.Empty(sut.Liste());
    }

    [Fact]
    public void Juste_oeufs()
    {
        var sut = new Allergies(1);
        var expected = new[] { Allergene.Oeufs };
        Assert.Equal(expected, sut.Liste());
    }

    [Fact]
    public void Juste_arachides()
    {
        var sut = new Allergies(2);
        var expected = new[] { Allergene.Arachides };
        Assert.Equal(expected, sut.Liste());
    }

    [Fact]
    public void Juste_fraises()
    {
        var sut = new Allergies(8);
        var expected = new[] { Allergene.Fraises };
        Assert.Equal(expected, sut.Liste());
    }

    [Fact]
    public void Oeufs_et_arachides()
    {
        var sut = new Allergies(3);
        var expected = new[] { Allergene.Oeufs, Allergene.Arachides };
        Assert.Equal(expected, sut.Liste());
    }

    [Fact]
    public void Plus_qu_aux_oeufs_mais_pas_aux_arachides()
    {
        var sut = new Allergies(5);
        var expected = new[] { Allergene.Oeufs, Allergene.Crustaces };
        Assert.Equal(expected, sut.Liste());
    }

    [Fact]
    public void Beaucoup_de_choses()
    {
        var sut = new Allergies(248);
        var expected = new[] { Allergene.Fraises, Allergene.Tomates, Allergene.Chocolat, Allergene.Pollen, Allergene.Chats };
        Assert.Equal(expected, sut.Liste());
    }

    [Fact]
    public void Tout()
    {
        var sut = new Allergies(255);
        var expected = new[] { Allergene.Oeufs, Allergene.Arachides, Allergene.Crustaces, Allergene.Fraises, Allergene.Tomates, Allergene.Chocolat, Allergene.Pollen, Allergene.Chats };
        Assert.Equal(expected, sut.Liste());
    }

    [Fact]
    public void Allergene_pas_dans_la_liste_des_allergenes()
    {
        var sut = new Allergies(509);
        var expected = new[] { Allergene.Oeufs, Allergene.Crustaces, Allergene.Fraises, Allergene.Tomates, Allergene.Chocolat, Allergene.Pollen, Allergene.Chats };
        Assert.Equal(expected, sut.Liste());
    }
}