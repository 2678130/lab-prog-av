using System;
using System.Collections.Generic;
using System.Linq;

public enum Allergene
{
    Oeufs = 1, //1
    Arachides = 2, //2
    Crustaces = 4, //4
    Fraises = 8, //8
    Tomates = 16, //16
    Chocolat = 32, //32
    Pollen = 64, //64
    Chats = 128 //128
}

public class Allergies
{
    private byte _mask;
    public Allergies(int mask)
    {
        this._mask = (byte)mask;
    }

    public bool EstAllergique(Allergene allergen)
    {
        return ((int)allergen & this._mask) > 0;
    }

    public Allergene[] Liste()
    {
        var values = (Allergene[])Enum.GetValues(typeof(Allergene));
        return values.Select(v => v).Where(v => ((int)v & this._mask) > 0).ToArray();
    }
}