using System;
using System.Collections.Generic;
using System.Linq;

public class HighScores
{
    private List<int> listScore = new List<int>();

    public HighScores(List<int> list)
    {
        this.listScore = list;
    }

    // Les scores dans l'ordre original
    public List<int> Scores()
    {
        return this.listScore;
    }

    // Literalement le dernier score entrer
    public int Dernier()
    {
        return this.listScore.Last();
    }

    // Meilleur 1 ?
    public int MeilleurScorePersonnel()
    {
        int max = 0;
        foreach(var score in this.listScore)
        {
            max = score > max ? score : max;
        }
        return max;
    }

    // Trois meilleure
    public List<int> TroisMeilleursScoresPersonnel()
    {
        var sortedList = this.listScore.OrderByDescending(i => i).ToList();
        var upperBound = Math.Min(3, sortedList.Count);
        
        return sortedList.GetRange(0, upperBound);
    }
}