using System.Collections.Generic;
using Xunit;

public class HighScoresTest
{
    [Fact]
    public void Liste_des_scores()
    {
        var sut = new HighScores(new List<int> { 30, 50, 20, 70 });
        Assert.Equal(new List<int> { 30, 50, 20, 70 }, sut.Scores());
    }

    [Fact]
    public void Dernier_score()
    {
        var sut = new HighScores(new List<int> { 100, 0, 90, 30 });
        Assert.Equal(30, sut.Dernier());
    }

    [Fact]
    public void Meilleur_personnel()
    {
        var sut = new HighScores(new List<int> { 40, 100, 70 });
        Assert.Equal(100, sut.MeilleurScorePersonnel());
    }

    [Fact]
    public void Meilleurs_3_personnels_d_une_liste_de_scores()
    {
        var sut = new HighScores(new List<int> { 10, 30, 90, 30, 100, 20, 10, 0, 30, 40, 40, 70, 70 });
        Assert.Equal(new List<int> { 100, 90, 70 }, sut.TroisMeilleursScoresPersonnel());
    }

    [Fact]
    public void Meilleurs_personnel_du_plus_haut_au_plus_bas()
    {
        var sut = new HighScores(new List<int> { 20, 10, 30 });
        Assert.Equal(new List<int> { 30, 20, 10 }, sut.TroisMeilleursScoresPersonnel());
    }

    [Fact]
    public void Meilleurs_personnel_quand_il_y_a_egalite()
    {
        var sut = new HighScores(new List<int> { 40, 20, 40, 30 });
        Assert.Equal(new List<int> { 40, 40, 30 }, sut.TroisMeilleursScoresPersonnel());
    }

    [Fact]
    public void Meilleur_personnel_quand_il_y_en_a_moins_de_3()
    {
        var sut = new HighScores(new List<int> { 30, 70 });
        Assert.Equal(new List<int> { 70, 30 }, sut.TroisMeilleursScoresPersonnel());
    }

    [Fact]
    public void Meilleur_personnel_quand_il_y_en_juste_1()
    {
        var sut = new HighScores(new List<int> { 40 });
        Assert.Equal(new List<int> { 40 }, sut.TroisMeilleursScoresPersonnel());
    }
}