# High Scores

Gérez la liste des meilleurs scores d'un joueur. La liste des meilleurs scores est appelée [high score](https://fr.wikipedia.org/wiki/Score_(nombre_de_points)).

Votre tâche consiste à créer un composant qui permet de gérer les `High Scores` pour le jeu vidéo d'arcade [Frogger](https://fr.wikipedia.org/wiki/Frogger). Votre tâche consiste à écrire les méthodes qui retournent le meilleur score de la liste, le dernier score ajouté et les trois meilleurs scores.

## Exécution des tests

Pour exécuter les tests, exécutez la commande `dotnet test` à partir du répertoire de l'exercice.

Initialement, seul le premier test sera activé. C'est pour vous encourager à résoudre l'exercice une étape à la fois.
Une fois que vous avez réussi le premier test, supprimez la propriété `Skip` du test suivant et travaillez à faire passer ce prochain test.
Une fois qu'aucun des tests n'est ignoré et qu'ils réussissent tous, vous devez soumettre votre solution.
