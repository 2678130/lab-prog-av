using Xunit;

public class CouleurDeResistanceTest
{
    [Fact]
    public void Noir()
    {
        Assert.Equal(0, CouleurDeResistance.CodePourCouleur("noir"));
    }

    [Fact]
    public void Blanc()
    {
        Assert.Equal(9, CouleurDeResistance.CodePourCouleur("blanc"));
    }

    [Fact]
    public void Orange()
    {
        Assert.Equal(3, CouleurDeResistance.CodePourCouleur("orange"));
    }

    [Fact]
    public void Couleurs()
    {
        Assert.Equal(new[] { "noir", "marron", "rouge", "orange", "jaune", "vert", "bleu", "violet", "gris", "blanc" }, CouleurDeResistance.Couleurs());
    }
}