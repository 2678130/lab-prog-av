# Couleurs des résistances

La valeur des résistances peut être indiquée sur le composant sous forme de trois à six anneaux de couleurs où chaque couleur correspond à un nombre. Les deux premiers indiquent deux chiffres (un chiffre correspondant à une couleur).

Code des couleurs pour les résistances:

- Noir: 0
- Marron: 1
- Rouge: 2
- Orange: 3
- Jaune: 4
- Vert: 5
- Bleu: 6
- Violet: 7
- Gris: 8
- Blanc: 9

Pour plus d'informations sur le codage des couleurs des résistances, consultez [CEI 60757 sur Wikipedia](https://fr.wikipedia.org/wiki/CEI_60757)

## Exécution des tests

Pour exécuter les tests, exécutez la commande `dotnet test` à partir du répertoire de l'exercice.

Initialement, seul le premier test sera activé. C'est pour vous encourager à résoudre l'exercice une étape à la fois.
Une fois que vous avez réussi le premier test, supprimez la propriété `Skip` du test suivant et travaillez à faire passer ce prochain test.
Une fois qu'aucun des tests n'est ignoré et qu'ils réussissent tous, vous devez soumettre votre solution.
