﻿using System;

public static class CouleurDeResistance
{
    public enum Couleur
    {
        noir,
        marron,
        rouge,
        orange,
        jaune,
        vert,
        bleu,
        violet,
        gris,
        blanc,
    }

    public static int CodePourCouleur(string couleur)
    {
        return (int)Enum.Parse(typeof(Couleur), couleur);
    }

    public static string[] Couleurs()
    {
        return Enum.GetNames(typeof(Couleur));
    }
}