using System;

public static class AnneeBissextile
{
    /*
        chaque année divisible par 4
        sauf chaque année divisible par 100
        sauf si l'année est également divisible par 400
    */

    public static bool EstUneAnneeBissextile(int annee)
    {
        // cas pas divisble par 4
        if(annee % 4 != 0)
        {
            return false;
        }

        // cas divisble par 100 et 400
        if(annee % 100 == 0 && annee % 400 == 0)
        {
            return true;
        }

        // cas divisible uniquement par 100
        if(annee % 100 == 0)
        {
            return false;
        }

        // divisible par 4 sans edge case
        return true;
    }
}