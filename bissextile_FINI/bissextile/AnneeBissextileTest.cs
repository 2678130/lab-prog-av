using Xunit;

public class AnneeBissextileTest
{
    [Fact]
    public void Annee_pas_divisible_par_4_dans_une_année_commune()
    {
        Assert.False(AnneeBissextile.EstUneAnneeBissextile(2015));
    }

    [Fact]
    public void Annee_divisible_par_2_pas_divisible_par_4_dans_une_année_commune()
    {
        Assert.False(AnneeBissextile.EstUneAnneeBissextile(1970));
    }

    [Fact]
    public void Annee_divisible_par_4_pas_divisible_par_100_dans_annee_bissextile()
    {
        Assert.True(AnneeBissextile.EstUneAnneeBissextile(1996));
    }

    [Fact]
    public void Annee_divisible_par_4_et_5_est_encore_une_annee_bissextile()
    {
        Assert.True(AnneeBissextile.EstUneAnneeBissextile(1960));
    }

    [Fact]
    public void Annee_divisible_par_100_pas_divisible_par_400_dans_une_année_commune()
    {
        Assert.False(AnneeBissextile.EstUneAnneeBissextile(2100));
    }

    [Fact]
    public void Annee_divisible_par_100_but_pas_par_3_est_encore_pas_une_annee_bissextile()
    {
        Assert.False(AnneeBissextile.EstUneAnneeBissextile(1900));
    }

    [Fact]
    public void Annee_divisible_par_400_dans_annee_bissextile()
    {
        Assert.True(AnneeBissextile.EstUneAnneeBissextile(2000));
    }

    [Fact]
    public void Annee_divisible_par_400_mais_pas_par_125_est_encore_une_annee_bissextile()
    {
        Assert.True(AnneeBissextile.EstUneAnneeBissextile(2400));
    }

    [Fact]
    public void Annee_divisible_par_200_pas_divisible_par_400_dans_une_année_commune()
    {
        Assert.False(AnneeBissextile.EstUneAnneeBissextile(1800));
    }
}