# Année bissextile

Vous recevez une année, veuillez indiquer s'il s'agit d'une année bissextile.

Une année bissextile dans le calendrier grégorien se produit:

```text
chaque année divisible par 4
   sauf chaque année divisible par 100
     sauf si l'année est également divisible par 400
```

Par exemple, 1997 n'est pas une année bissextile, mais 1996 l'est. 1900 n'est pas une année bissextile, mais 2000 l'est.

## Notes

La classe DateTime en C# fournit la une méthode [IsLeapYear](https://docs.microsoft.com/fr-ca/dotnet/api/system.datetime.isleapyear?view=netframework-4.8). Vous devez faire comme si celle-ci n'existait pas pour la mise en œuvre de cet exercice.

## Exécution des tests

Pour exécuter les tests, exécutez la commande `dotnet test` à partir du répertoire de l'exercice.

Initialement, seul le premier test sera activé. C'est pour vous encourager à résoudre l'exercice une étape à la fois.
Une fois que vous avez réussi le premier test, supprimez la propriété `Skip` du test suivant et travaillez à faire passer ce prochain test.
Une fois qu'aucun des tests n'est ignoré et qu'ils réussissent tous, vous devez soumettre votre solution.
