﻿using System;

public static class DeuxPourUn
{
    // Pour exécuter les tests, vous devez d'abord vous assurer que la méthode Parle
    // peut être appelé à la fois sans aucun argument et également en passant un argument de type string.
    public static string Parle(string parle = "toi")
    {
        return $"Un pour {parle}, un pour moi.";
    }
}
