using System;
using System.Linq;
using Xunit;

public class DeuxPourUnTests
{
    [Fact]
    public void Pas_de_nom()
    {
        Assert.Equal("Un pour toi, un pour moi.", DeuxPourUn.Parle());
    }

    [Fact]
    public void Avec_un_nom()
    {
        Assert.Equal("Un pour Adèle, un pour moi.", DeuxPourUn.Parle("Adèle"));
    }

    [Fact]
    public void Avec_un_autre_nom()
    {
        Assert.Equal("Un pour Benoît, un pour moi.", DeuxPourUn.Parle("Benoît"));
    }

    [Fact]
    public void Avec_un_nom_aleatoire()
    {
        var random = new Random();
        var nom = RandomString(10);

        Assert.Equal($"Un pour {nom}, un pour moi.", DeuxPourUn.Parle(nom));
                
        string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}