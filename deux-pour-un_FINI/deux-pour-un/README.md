# Deux pour un

Deux pour un. Un pour toi et un pour moi.

Vous recevez un nom, renvoyez une chaîne avec le message:

```texte
Un pour X, un pour moi.
```

Où X est le prénom.

Cependant, si le nom est manquant (ou vide), renvoyez la chaîne:

```texte
Un pour toi, un pour moi.
```

Voici quelques exemples:

| Nom       | Chaîne à renvoyer
|: -------  |: ------------------
| Adèle     | Un pour Adèle, un pour moi.
| Benoît    | Un pour Benoît, un pour moi.
|           | Un pour toi, un pour moi.
| Carole    | Un pour Carole, un pour moi.

## Exécution des tests

Pour exécuter les tests, exécutez la commande `dotnet test` à partir du répertoire de l'exercice.

Initialement, seul le premier test sera activé. C'est pour vous encourager à résoudre l'exercice une étape à la fois.
Une fois que vous avez réussi le premier test, supprimez la propriété `Skip` du test suivant et travaillez à faire passer ce prochain test.
Une fois qu'aucun des tests n'est ignoré et qu'ils réussissent tous, vous devez soumettre votre solution.
