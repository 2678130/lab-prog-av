# Analyseur syntaxique (combinaisons d’unités lexicales)

Étant donné une chaîne contenant des crochets tableaux `[]`, des accolades `{}`, des parenthèses `()`, ou toute combinaison de ceux-ci, vérifiez que toutes les paires correspondent et sont imbriqué correctement.

En d'autre mots, vous devez vérifier si chaque parenthèse fermante « ] } ) » correspond à une parenthèse ouvrante « ( { [ » précédente.

## Exécution des tests

Pour exécuter les tests, exécutez la commande `dotnet test` à partir du répertoire de l'exercice.

Initialement, seul le premier test sera activé. C'est pour vous encourager à résoudre l'exercice une étape à la fois.
Une fois que vous avez réussi le premier test, supprimez la propriété `Skip` du test suivant et travaillez à faire passer ce prochain test.
Une fois qu'aucun des tests n'est ignoré et qu'ils réussissent tous, vous devez soumettre votre solution.
