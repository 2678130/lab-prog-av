using Xunit;

public class CombinaisonsUnitesLexicalesTest
{
    [Fact]
    public void Crochets_apparies()
    {
        var valeur = "[]";
        Assert.True(CombinaisonsUnitesLexicales.Valide(valeur));
    }

    [Fact]
    public void String_vide()
    {
        var valeur = "";
        Assert.True(CombinaisonsUnitesLexicales.Valide(valeur));
    }

    [Fact]
    public void Crochets_non_apparies()
    {
        var valeur = "[[";
        Assert.False(CombinaisonsUnitesLexicales.Valide(valeur));
    }

    [Fact]
    public void Crochets_mal_ordonnes()
    {
        var valeur = "}{";
        Assert.False(CombinaisonsUnitesLexicales.Valide(valeur));
    }

    [Fact]
    public void Fermeture_incorrect()
    {
        var valeur = "{]";
        Assert.False(CombinaisonsUnitesLexicales.Valide(valeur));
    }

    [Fact]
    public void Crochets_apparies_avec_espace()
    {
        var valeur = "{ }";
        Assert.True(CombinaisonsUnitesLexicales.Valide(valeur));
    }

    [Fact]
    public void Partiellement_correct()
    {
        var valeur = "{[])";
        Assert.False(CombinaisonsUnitesLexicales.Valide(valeur));
    }

    [Fact]
    public void Crochets_imbriques_simples()
    {
        var valeur = "{[]}";
        Assert.True(CombinaisonsUnitesLexicales.Valide(valeur));
    }

    [Fact]
    public void Plusieurs_crochets_imbriques_simples()
    {
        var valeur = "{}[]";
        Assert.True(CombinaisonsUnitesLexicales.Valide(valeur));
    }

    [Fact]
    public void crochets_apparies_et_imbrique()
    {
        var valeur = "([{}({}[])])";
        Assert.True(CombinaisonsUnitesLexicales.Valide(valeur));
    }

    [Fact]
    public void Fermeture_non_ouvertes()
    {
        var valeur = "{[)][]}";
        Assert.False(CombinaisonsUnitesLexicales.Valide(valeur));
    }

    [Fact]
    public void Non_apparies_imbriques()
    {
        var valeur = "([{])";
        Assert.False(CombinaisonsUnitesLexicales.Valide(valeur));
    }

    [Fact]
    public void Imbriques_apparies_mais_incorrects()
    {
        var valeur = "[({]})";
        Assert.False(CombinaisonsUnitesLexicales.Valide(valeur));
    }

    [Fact]
    public void Apparies_et_incomplets()
    {
        var valeur = "{}[";
        Assert.False(CombinaisonsUnitesLexicales.Valide(valeur));
    }

    [Fact]
    public void Trop_de_chrochets_fermants()
    {
        var valeur = "[]]";
        Assert.False(CombinaisonsUnitesLexicales.Valide(valeur));
    }

    [Fact]
    public void Expression_mathematique()
    {
        var valeur = "(((105 + 446.81) * 42) - 22)/7";
        Assert.True(CombinaisonsUnitesLexicales.Valide(valeur));
    }

    [Fact]
    public void Expression_mathematique_Einstein()
    {
        var valeur = "[E=mc^2]";
        Assert.True(CombinaisonsUnitesLexicales.Valide(valeur));
    }

    [Fact]
    public void LaTeX_mathematiques()
    {
        var valeur = @"\begin{subequations}\label{eq:Maxwell}Maxwell's equations:\begin{align}B'&=-\nabla \times E,\label{eq:MaxB} \\ E'&=\nabla \times B - 4\pi j, \label{eq:MaxE}\end{align}\end{subequations}";        
        Assert.True(CombinaisonsUnitesLexicales.Valide(valeur));
    }
}