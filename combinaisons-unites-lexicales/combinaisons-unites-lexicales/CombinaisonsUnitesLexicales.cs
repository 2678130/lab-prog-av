using System;
using System.Collections.Generic;

public static class CombinaisonsUnitesLexicales
{
    // 40-41   (  )
    // 91-93   [  ]
    // 123-125 {  }
    public static bool Valide(string code)
    {
        // leverage the stack
        // 40, 91 123 => add
        // 41, 93 125 => remove
        // add then remove, check if pair is valid when removing
        // make sure stack is not empty when removing
        // make sure stack is empty when finished

        var validationStack = new Stack<char>();
        foreach(var letter in code)
        {
            var charValue = (int)letter;
            // add to the stack
            if(charValue == 40 || charValue == 91 || charValue == 123)
            {
                validationStack.Push(letter);
                continue;
            }

            if(charValue == 41 || charValue == 93 || charValue == 125)
            {
                // stack is already empty, nothing to remove
                if(validationStack.Count == 0)
                {
                    return false;
                }
                // remove last value
                var previousValue = validationStack.Pop();

                // check if it's a matching pair
                if(Math.Abs(previousValue - charValue) > 4)
                {
                    return false;
                }
            }
        }

        // stack is empty when finished
        if(validationStack.Count > 0)
        {
            return false;
        }

        // all good
        return true;
    }
}
