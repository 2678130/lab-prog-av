using System;
using System.Collections.Generic;
using System.Linq;

public class Robot
{
    private string _name = null;
    private const int NAME_GENERATION_LIMIT = 5;
    private static HashSet<string> registry = new HashSet<string>();
    public string Nom
    {
        get
        {
            if (this._name is null)
            {
                this.Reinitialiser();
                registry.Add(this._name);
            }
            return this._name;
        }
    }
    public void Reinitialiser()
    {
        int iterationCount = 0;
        do
        {
            GenerateName();
            if(iterationCount > NAME_GENERATION_LIMIT)
            {
                throw new Exception("Error, generation limit reached (5)");
            }
        }
        while (registry.Contains(this._name));
    }
    private void GenerateName()
    {
        var name = new char[5];
        var rand = new Random();
        // Format => AB123
        // 65 to 90 => Letters
        // 48 to 57 => Numbers
        // Random name generation
        foreach (var index in Enumerable.Range(0, name.Length))
        {
            // index 0 - 1 => 65 to 90
            if (index < 2)
            {
                name[index] = (char)rand.Next(65, 91);
                continue;
            }
            // numbers
            name[index] = (char)rand.Next(48, 58);
        }
        this._name = new string(name);
    }
}