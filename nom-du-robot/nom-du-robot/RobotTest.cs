using System.Collections.Generic;
using Xunit;

public class RobotTest
{
    private readonly Robot robot = new Robot();

    [Fact]
    public void Robot_a_un_nom()
    {
        Assert.Matches(@"^[A-Z]{2}\d{3}$", robot.Nom);
    }

    [Fact]
    public void Le_nom_est_le_meme_à_chaque_fois()
    {
        Assert.Equal(robot.Nom, robot.Nom);
    }

    [Fact]
    public void Differents_robots_ont_des_noms_differents()
    {
        var robot2 = new Robot();
        Assert.NotEqual(robot2.Nom, robot.Nom);
    }

    [Fact]
    public void Apres_reinitialisation_le_nom_est_different()
    {
        var nomOriginal = robot.Nom;
        robot.Reinitialiser();
        Assert.NotEqual(nomOriginal, robot.Nom);
    }

    [Fact]
    public void Apres_reinitialisation_le_nom_est_valide()
    {
        robot.Reinitialiser();
        Assert.Matches(@"^[A-Z]{2}\d{3}$", robot.Nom);
    }

    [Fact]
    public void Les_noms_des_robots_sont_uniques()
    {
        var noms = new HashSet<string>();
        for (int i = 0; i < 10_000; i++) {
            var robot = new Robot();
            Assert.True(noms.Add(robot.Nom));
        }
    }
}
