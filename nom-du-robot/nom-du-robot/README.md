# Nom du robot

Gérez les paramètres d'usine (ou paramètre par défaut) du robot.

Lorsque les robots quittent l'usine, ils n'ont pas de nom.

La première fois que vous les démarrez, un nom aléatoire est généré au format de deux lettres majuscules suivies de trois chiffres, comme RX837 ou BC811.

De temps en temps, vous devez réinitialiser un robot à ses paramètres d'usine, ce qui signifie que leur nom est effacé. La prochaine fois que vous demanderez, il vous répondra avec un nouveau nom aléatoire.

## Notes de mise en œuvre

Les noms doivent être aléatoires: ils ne doivent pas suivre une séquence prévisible. Des noms aléatoires signifient un risque de collision. Votre solution doit garantir que chaque robot existant a un nom unique.

## Exécution des tests

Pour exécuter les tests, exécutez la commande `dotnet test` à partir du répertoire de l'exercice.

Initialement, seul le premier test sera activé. C'est pour vous encourager à résoudre l'exercice une étape à la fois.
Une fois que vous avez réussi le premier test, supprimez la propriété `Skip` du test suivant et travaillez à faire passer ce prochain test.
Une fois qu'aucun des tests n'est ignoré et qu'ils réussissent tous, vous devez soumettre votre solution.
