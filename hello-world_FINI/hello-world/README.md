# Hello World

Exercice d'introduction classique. Dites simplement "Hello, World!".

["Hello, World!"](https://fr.wikipedia.org/wiki/Hello_world) sont les mots traditionnellement écrits par un programme informatique simple dont le but est de faire la démonstration rapide de son exécution sans erreur.

Les objectifs sont :

- Ecrivez une fonction qui retourne la chaîne "Hello, World!".
- Exécutez la suite de tests et assurez-vous qu'elle réussit.
- Soumettez votre solution et vérifiez-la sur le site.

## Exécution des tests

Pour exécuter les tests, exécutez la commande `dotnet test` à partir du répertoire de l'exercice.

Initialement, seul le premier test sera activé. C'est pour vous encourager à résoudre l'exercice une étape à la fois.
Une fois que vous avez réussi le premier test, supprimez la propriété `Skip` du test suivant et travaillez à faire passer ce prochain test.
Une fois qu'aucun des tests n'est ignoré et qu'ils réussissent tous, vous devez soumettre votre solution.
