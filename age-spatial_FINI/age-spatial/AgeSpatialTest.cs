using Xunit;

public class AgeSpatialTest
{
    [Fact]
    public void Age_sur_la_terre()
    {
        var sut = new AgeSpatial(1_000_000_000);
        Assert.Equal(31.69, sut.SurLaTerre(), precision: 2);
    }

    [Fact]
    public void Age_sur_mercure()
    {
        var sut = new AgeSpatial(2_134_835_688);
        Assert.Equal(280.88, sut.SurMercure(), precision: 2);
    }

    [Fact]
    public void Age_sur_venus()
    {
        var sut = new AgeSpatial(189_839_836);
        Assert.Equal(9.78, sut.SurVenus(), precision: 2);
    }

    [Fact]
    public void Age_sur_mars()
    {
        var sut = new AgeSpatial(2_129_871_239);
        Assert.Equal(35.88, sut.SurMars(), precision: 2);
    }

    [Fact]
    public void Age_sur_jupiter()
    {
        var sut = new AgeSpatial(901_876_382);
        Assert.Equal(2.41, sut.SurJupiter(), precision: 2);
    }

    [Fact]
    public void Age_sur_saturne()
    {
        var sut = new AgeSpatial(2_000_000_000);
        Assert.Equal(2.15, sut.SurSaturne(), precision: 2);
    }

    [Fact]
    public void Age_sur_uranus()
    {
        var sut = new AgeSpatial(1_210_123_456);
        Assert.Equal(0.46, sut.SurUranus(), precision: 2);
    }

    [Fact]
    public void Age_sur_neptune()
    {
        var sut = new AgeSpatial(1_821_023_456);
        Assert.Equal(0.35, sut.SurNeptune(), precision: 2);
    }
}