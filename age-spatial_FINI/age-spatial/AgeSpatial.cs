using System;

public class AgeSpatial
{
    //# Âge spatial

    //    Vous recevez un âge en secondes, calculez l'âge de quelqu'un sur:

    //- Terre  : période orbitale de 365.25 jours terrestres, ou 31 557 600 secondes
    //- Mercure: période orbitale de 0,2408467 année terrestre
    //- Vénus  : période orbitale de 0,61519726 année terrestre
    //- Mars   : période orbitale de 1,8808158 années terrestres
    //- Jupiter: période orbitale de 11,862615 années terrestres
    //- Saturne: période orbitale de 29,447498 années terrestres
    //- Uranus : période orbitale de 84,016846 années terrestres
    //- Neptune: période orbitale de 164,79132 années terrestres

    //Donc, si on vous dit que quelqu'un a un âge de 1 000 000 000 de secondes, vous devriez pouvoir dire qu'il a 31,69 années terrestres.

    //## Exécution des tests

    //Pour exécuter les tests, exécutez la commande `dotnet test` à partir du répertoire de l'exercice.

    //Initialement, seul le premier test sera activé. C'est pour vous encourager à résoudre l'exercice une étape à la fois.
    //Une fois que vous avez réussi le premier test, supprimez la propriété `Skip` du test suivant et travaillez à faire passer ce prochain test.
    //Une fois qu'aucun des tests n'est ignoré et qu'ils réussissent tous, vous devez soumettre votre solution.

    private int secondes;
    private double yearInSecondsEarth = 31_557_600;
    private double mercureConversionMultiplier = 0.2408467;
    private double venusConversionMultiplier = 0.61519726;
    private double marsConversionMultiplier = 1.8808158;
    private double jupiterConversionMultiplier = 11.862615;
    private double saturnConversionMultiplier = 29.447498;
    private double uranusConversionMultiplier = 84.016846;
    private double neptuneConversionMultiplier = 164.79132;

    public AgeSpatial(int secondes)
    {
        this.secondes = secondes;
    }

    public double SurLaTerre()
    {
        return this.secondes / this.yearInSecondsEarth;
    }

    public double SurMercure()
    {
        return this.SurLaTerre() / this.mercureConversionMultiplier;
    }

    public double SurVenus()
    {
        return this.SurLaTerre() / this.venusConversionMultiplier;
    }

    public double SurMars()
    {
        return this.SurLaTerre() / this.marsConversionMultiplier;
    }

    public double SurJupiter()
    {
        return this.SurLaTerre() / this.jupiterConversionMultiplier;
    }

    public double SurSaturne()
    {
        return this.SurLaTerre() / this.saturnConversionMultiplier;
    }

    public double SurUranus()
    {
        return this.SurLaTerre() / this.uranusConversionMultiplier;
    }

    public double SurNeptune()
    {
        return this.SurLaTerre() / this.neptuneConversionMultiplier;
    }
}