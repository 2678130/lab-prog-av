﻿using System;
using Devoir2.Model;

namespace Devoir2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // creer professeur et etudiants
            var p1 = new Professeur(1068874, "John Chi");
            var p2 = new Professeur(9999999, "Bill Gates");
            var e1 = new Etudiant(0000001, "Steve Jobs");
            var e2 = new Etudiant(0000002, "Charlie Sheen");
            var e3 = new Etudiant(0000003, "Adam West");

            // creer les cours
            var ord12345 = new Cours("Intro au C#", "ORD12345", "F3320", 4);
            var ord88745 = new Cours("DB 101", "ORD88745", "F2240", 4);

            // cours ord12345
            ord12345.AjoutePersonne(p1);
            ord12345.AjoutePersonne(e1);
            ord12345.AjoutePersonne(e2);
            ord12345.AjoutePersonne(e3);

            // cours ord88745
            ord88745.AjoutePersonne(p2);
            ord88745.AjoutePersonne(e1);
            ord88745.AjoutePersonne(e2);
            ord88745.AjoutePersonne(e3);

            var separateur = "------------------------------------------";
            Console.WriteLine(separateur);
            Console.WriteLine(ord12345.ToString());
            Console.WriteLine(separateur);
            Console.WriteLine(ord88745.ToString());
            Console.WriteLine(separateur);

            // Console stuff
            Personne p = null;
            p = GetPersonneIDDeLaConsole(ord12345);
            Console.WriteLine(p.ToString() + "\n");
            p = GetPersonneIDDeLaConsole(ord12345);
            Console.WriteLine(p.ToString() + "\n");
            p = GetPersonneIDDeLaConsole(ord88745);
            Console.WriteLine(p.ToString() + "\n");
            p = GetPersonneIDDeLaConsole(ord88745);
            Console.WriteLine(p.ToString() + "\n");

            Console.ReadLine();
        }

        public static Personne GetPersonneIDDeLaConsole(Cours cour)
        {
            do
            {
                Console.WriteLine("Recherche une personne par id ou nom dans le cours " + cour.CoteDeCours);
                Console.WriteLine("Nom ou Id de la personne : ");
                string nomOuId = Console.ReadLine().Trim();
                try
                {
                    var isNumeric = int.TryParse(nomOuId, out int id);

                    if (isNumeric)
                    {
                        return cour[id];
                    }

                    return cour[nomOuId];
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message + "\n");
                }
            }
            while (true);
        }
    }
}
