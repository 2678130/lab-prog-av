﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Devoir2.Exceptions;

namespace Devoir2.Model
{
    public class Cours : IEnumerable<Personne>
    {
        private readonly HashSet<Personne> _personnes;

        public Cours(string nom, string coteDeCours, string salleDeCours, int heuresParSemaine)
        {
            this.Nom = ValideChaine(nom);
            this.CoteDeCours = ValideChaine(coteDeCours);
            this.SalleDeCours = ValideChaine(salleDeCours);
            this.HeuresParSemaine = ValideInt(heuresParSemaine);
            this.HeuresParSemaine = heuresParSemaine;
            this._personnes = new HashSet<Personne>();
            this.DateDebut = new DateTime(2021, 01, 01);
            this.DateFin = new DateTime(2021, 05, 01);
        }

        public string Nom { get; }

        public string CoteDeCours { get; }

        public string SalleDeCours { get; }

        public int HeuresParSemaine { get; }

        public DateTime DateDebut { get; }

        public DateTime DateFin { get; }

        // indexeurs
        public Personne this[int id]
        {
            get
            {
                id = ValideInt(id);
                Personne p = this.Where(personne => personne.Id == id).FirstOrDefault();

                if (p == null)
                {
                    throw new PersonneNonExistanteException(null, id);
                }

                return p;
            }
        }

        public Personne this[string nom]
        {
            get
            {
                nom = ValideChaine(nom);
                Personne p = this.Where(personne => personne.Nom.ToLower() == nom.ToLower()).FirstOrDefault();

                if (p == null)
                {
                    throw new PersonneNonExistanteException(nom);
                }

                return p;
            }
        }

        // overrides ToString
        public override string ToString()
        {
            var message = new StringBuilder();

            // _Nom (_CoteDeCours)
            message.AppendLine(string.Format("{0} ({1})", Nom, CoteDeCours));

            // -tab Salle de classe : _SalleDeCours
            message.AppendLine(string.Format("\tSalle de classe : {0}", SalleDeCours));

            // -tab Heures par semaine : _HeuresParSemaine
            message.AppendLine(string.Format("\tHeures par semaine : {0}", HeuresParSemaine));

            // -tab Du _DateDebut au _DateFin
            message.AppendLine(string.Format("\tDu {0} au {1}", this.DateDebut.ToShortDateString(), this.DateFin.ToShortDateString()));

            // => list des Professeurs
            var listProfesseurs = this.Where(personne => personne is Professeur).ToList();

            // -tab Professeurs (_count)
            message.AppendLine(string.Format("\tProfesseurs ({0}) :", listProfesseurs.Count));

            // -tab -tab lists professeur
            foreach (var professeur in listProfesseurs)
            {
                message.AppendLine(string.Format("\t\t{0}", professeur));
            }

            // => list des Etudiants
            var listEtudiants = this.Where(personne => personne is Etudiant).ToList();

            // -tab Etudiants (count)
            message.AppendLine(string.Format("\tEtudiants ({0}) :", listEtudiants.Count));

            // -tab -tab lists etudiants
            foreach (var etudiant in listEtudiants)
            {
                message.AppendLine(string.Format("\t\t{0}", etudiant));
            }

            return message.ToString();
        }

        // Methode publique -- Ajoute + Retire
        public void AjoutePersonne(Personne p)
        {
            this._personnes.Add(p);
        }

        public void RetirePersonne(Personne p)
        {
            var retirer = this._personnes.Remove(p);

            if (!retirer)
            {
                throw new PersonneNonExistanteException(p.ToString());
            }
        }

        public void RetirePersonne(int id)
        {
            id = ValideInt(id);
            var personneARetirer = this[id];
            this.RetirePersonne(personneARetirer);
        }

        // Interface IEnumerable
        public IEnumerator<Personne> GetEnumerator()
        {
            return this._personnes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._personnes.GetEnumerator();
        }

        private string ValideChaine(string c)
        {
            if (string.IsNullOrWhiteSpace(c))
            {
                throw new ArgumentNullException(c);
            }

            return c;
        }

        private int ValideInt(int i)
        {
            if (i <= 0)
            {
                throw new ArgumentOutOfRangeException(i.ToString());
            }

            return i;
        }
    }
}
