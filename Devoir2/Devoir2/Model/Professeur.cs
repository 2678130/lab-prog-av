﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Devoir2.Model
{
    public class Professeur : Personne
    {
        public Professeur(int id, string nom)
            : base(id, nom)
        {
        }

        public override string ToString()
        {
            return string.Format("Professeur {0} ({1})", Nom, Id.ToString("0000000"));
        }
    }
}
