﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Devoir2.Model
{
    public class Etudiant : Personne
    {
        public Etudiant(int id, string nom)
            : base(id, nom)
        {
        }

        public override string ToString()
        {
            var str = string.Format("Etudiant {0} ({1})", Nom, Id.ToString("0000000"));
            return str;
        }
    }
}
