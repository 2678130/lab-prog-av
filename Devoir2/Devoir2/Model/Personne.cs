﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Devoir2.Model
{
    public abstract class Personne : IEquatable<Personne>
    {
        protected Personne(int id, string nom)
        {
            if (string.IsNullOrWhiteSpace(nom))
            {
                throw new ArgumentNullException(nom.ToString());
            }

            if (id <= 0)
            {
                throw new ArgumentOutOfRangeException(id.ToString());
            }

            this.Nom = nom;
            this.Id = id;
        }

        public string Nom { get; }

        public int Id { get; }

        // Overrides
        public static bool operator ==(Personne p1, Personne p2)
        {
            if (object.ReferenceEquals(p1, null))
            {
                return true;
            }

            return p1.Equals(p2);
        }

        public static bool operator !=(Personne p1, Personne p2)
        {
            if (object.ReferenceEquals(p1, null))
            {
                return false;
            }

            return !p1.Equals(p2);
        }

        public override int GetHashCode()
        {
            return this.Id;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is Personne))
            {
                return false;
            }

            return this.Equals(obj as Personne);
        }

        public override string ToString()
        {
            return string.Format(" Nom: {0} ({1})", Nom, Id);
        }

        // Comparaison IEquatable
        public bool Equals([AllowNull] Personne personne)
        {
            if (personne == null)
            {
                return false;
            }

            return personne.Id == this.Id;
        }
    }
}
