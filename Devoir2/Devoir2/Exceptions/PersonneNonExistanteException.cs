﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Devoir2.Exceptions
{
    public class PersonneNonExistanteException : Exception
    {
        public readonly string Nom;
        public readonly int Id;

        public PersonneNonExistanteException(string nom = null, int id = -1)
            : base(string.Format("Personne non existante => nom:{0} ou id:{1}", nom, id))
        {
            this.Nom = nom;
            this.Id = id;
        }
    }
}
