using System.Collections.Generic;

namespace fusion
{
    public class Fusion
    {
        public static List<int> Fusionner(List<int> a, List<int> b)
        {
            var concatenation = new List<int>();
            var setNumero = new HashSet<int>();

            // copier la liste a et ajouter au hashset
            foreach(var nombre in a)
            {
                // hs {0, 1, 2, 3}
                concatenation.Add(nombre);
                setNumero.Add(nombre);
            }

            // ajouter les nombre de la liste b dans concatenation, et verifier que concatenation n'a pas le nombre en regardant dans le HashSet
            foreach(var nombre in b)
            {
                // hs {nombre ?}
                // liste b {4, 4}
                if (!setNumero.Contains(nombre))
                {
                    concatenation.Add(nombre);
                }
            }

            return concatenation;
        }
            
    }
}
