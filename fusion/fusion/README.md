# Fusion

Écrivez et testez une fonction qui concatène deux listes de nombres entiers. Vous devez comparer les deux
listes et ajouter les nombres de la liste 'b' à ceux de la liste de 'a' que lorsqu’ils ne s’y trouvent pas. Cette
fonction doit avoir la signature suivante :

    List<int> Fusionner(List<int> a, List<int> b);

Par exemple, la fusion de la liste a = { 0, 1, 2, 1, 1, 1, 3 } et de la liste b = { 4, 5, 6, 2, 10 } retournerait la
liste suivante :
            
    { 0, 1, 2, 1, 1, 1, 3, 4, 5, 6, 10 }
    { a                  , b - a       }

Attention de ne pas modifier les listes passées en tant que paramètres. Vous devrez créer une nouvelle liste
qui sera retournée.

## Exécution des tests

Pour exécuter les tests, exécutez la commande `dotnet test` à partir du répertoire de l'exercice.

Initialement, seul le premier test sera activé. C'est pour vous encourager à résoudre l'exercice une étape à la fois.
Une fois que vous avez réussi le premier test, supprimez la propriété `Skip` du test suivant et travaillez à faire passer ce prochain test.
Une fois qu'aucun des tests n'est ignoré et qu'ils réussissent tous, vous devez soumettre votre solution.
