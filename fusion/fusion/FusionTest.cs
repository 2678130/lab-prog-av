﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace fusion
{
    public class FusionTest
    {
        [Theory]
        [InlineData(new int[] { 0, 1, 2, 1, 1, 1, 3 }, new int[] { 4, 5, 6, 2, 10 })]
        public void Concatene_deux_listes_de_nombres_entiers(int[] a, int[] b)
        {
            var listeA = new List<int>(a);
            var listeB = new List<int>(b);
			
            var result = Fusion.Fusionner(listeA, listeB);

            Assert.Equal(11, result.Count);
        }
		
		[Theory]
        [InlineData(new int[] { 0, 1, 2, 1, 1, 1, 3 }, new int[] { 4, 5, 6, 2, 10 })]
        public void Concatene_deux_listes_de_nombres_entiers_chiffre_verifie_nombre_unique(int[] a, int[] b)
        {
            var listeA = new List<int>(a);
            var listeB = new List<int>(b);

            var result = Fusion.Fusionner(listeA, listeB);

            Assert.Equal(1, result.Count(x => x == 2));
        }
		
		[Theory]
        [InlineData(new int[] { 0, 1, 2, 1, 1, 1, 3 }, new int[] { 4, 5, 6, 2, 10 })]
        public void Concatene_deux_listes_de_nombres_entiers_avec_random(int[] a, int[] b)
        {
            var listeA = new List<int>(a);
            var listeB = new List<int>(b);

            var random = new Random().Next(11, 100);
            listeB.Add(random);

            var result = Fusion.Fusionner(listeA, listeB);

            Assert.Equal(12, result.Count);
            Assert.Equal(1, result.Count(x => x == random));
        }
    }
}
