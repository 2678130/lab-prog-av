using System;
using Xunit;

public class HammingTest
{
    [Fact]
    public void Brin_vide()
    {
        Assert.Equal(0, Hamming.Distance("", ""));
    }

    [Fact]
    public void Brins_identiques_a_lettre_unique()
    {
        Assert.Equal(0, Hamming.Distance("A", "A"));
    }

    [Fact]
    public void Brins_different_a_lettre_unique()
    {
        Assert.Equal(1, Hamming.Distance("G", "T"));
    }

    [Fact]
    public void Longs_brins_identiques()
    {
        Assert.Equal(0, Hamming.Distance("GGACTGAAATCTG", "GGACTGAAATCTG"));
    }

    [Fact]
    public void Longs_brins_differents()
    {
        Assert.Equal(9, Hamming.Distance("GGACGGATTCTG", "AGGACGGATTCT"));
    }

    [Fact]
    public void Erreur_le_premier_brin_est_plus_long()
    {
        Assert.Throws<ArgumentException>(() => Hamming.Distance("AATG", "AAA"));
    }

    [Fact]
    public void Erreur_le_deuxieme_brin_est_plus_long()
    {
        Assert.Throws<ArgumentException>(() => Hamming.Distance("ATA", "AGTG"));
    }

    [Fact]
    public void Erreur_le_premier_brin_est_vide()
    {
        Assert.Throws<ArgumentException>(() => Hamming.Distance("", "G"));
    }

    [Fact]
    public void Erreur_le_deuxieme_brin_est_vide()
    {
        Assert.Throws<ArgumentException>(() => Hamming.Distance("G", ""));
    }
}