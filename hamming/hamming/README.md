# Distance de Hamming

Calculez la [distance de Hamming](https://fr.wikipedia.org/wiki/Distance_de_Hamming) entre deux brins d'ADN.

Votre corps est composé de cellules qui contiennent de l'ADN. Ces cellules s'usent régulièrement et doivent être remplacées, ce qu'elles obtiennent en se divisant d'une cellule mère en deux cellules filles strictement identiques génétiquement. En fait, le corps humain moyen connaît environ 10 billiard (10*10^15) de divisions cellulaires dans une vie!

Lorsque les cellules se divisent, leur ADN se réplique également. Parfois, au cours de ce processus, des erreurs se produisent et des pièces d'ADN uniques sont encodées avec des informations incorrectes. Si nous comparons deux brins d'ADN et comptons les différences entre eux, nous pouvons voir combien d'erreurs se sont produites. C'est ce qu'on appelle la "distance de Hamming".

Nous lisons l'ADN en utilisant les lettres C, A, G et T. Deux brins pourraient ressembler à ceci:

     GAGCCTACTAACGGGAT
     CATCGTAATGACGGCCT
     ^ ^ ^ ^ ^ ^^

Ils ont 7 différences, et donc la distance de Hamming est de 7.

Le poids de Hamming est utilisé dans plusieurs disciplines comme la théorie de l'information, la théorie des codes et la cryptographie.

## Notes de mise en œuvre

La distance de Hamming n'est définie que pour des séquences de longueur égale, donc une tentative de la calculer entre des séquences de longueurs différentes ne devrait pas fonctionner. Le traitement général de cette situation devrait lancer une exception.

## Exécution des tests

Pour exécuter les tests, exécutez la commande `dotnet test` à partir du répertoire de l'exercice.

Initialement, seul le premier test sera activé. C'est pour vous encourager à résoudre l'exercice une étape à la fois.
Une fois que vous avez réussi le premier test, supprimez la propriété `Skip` du test suivant et travaillez à faire passer ce prochain test.
Une fois qu'aucun des tests n'est ignoré et qu'ils réussissent tous, vous devez soumettre votre solution.
