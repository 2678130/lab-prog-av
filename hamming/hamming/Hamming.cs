using System;

public static class Hamming
{
    public static int Distance(string premierBrin, string deuxiemeBrin)
    {
        // Lancer une exception si la distance entre premierBrin et deuxiemeBrin n'est pas la meme
        if(premierBrin.Length != deuxiemeBrin.Length)
        {
            throw new ArgumentException("Longueur n'est pas la meme");
        }

        int distanceHamming = 0;
        // Iterer sur chacun des carateres de la chaine
        for(var i = 0; i < premierBrin.Length; i++)
        {
            // Si le charatere a l'index i de premierBrin n'est pas le meme que le caratere de l'index i de deuxiemeBrin, j'augmente la distance de hamming de 1
            if(premierBrin[i] != deuxiemeBrin[i])
            {
                distanceHamming += 1;
            }
        }

        // retourne un int
        return distanceHamming;
    }
}