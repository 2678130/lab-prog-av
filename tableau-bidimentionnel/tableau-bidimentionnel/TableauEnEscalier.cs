﻿using System;
using System.Linq;

namespace tableau_bidimentionnel
{
    public class TableauEnEscalier
    {
        public static double[][] Produit(double[] a, double[] b)
        {
            var productArray = new double [a.Length][];
            foreach(var row in Enumerable.Range(0, a.Length))
            {
                // each row of product array => (array b where every value of b multiplied by a[row])
                // b.Select... equivalent of array.map(value => value * row) in javascript
                productArray[row] = b.Select(value => value * a[row]).ToArray();
            }
            return productArray;
        }
    }
}
