# Tableau bidimentionnel

## a) Tableau rectangle

Écrivez et testez la fonction suivante:

    double[,] Produit(double[] a , double[] b);

La fontion renvoie le produit externe des éléments de a avec les éléments de b. Par exemple,  si a est
représenté par le tableau { 2.2, 3.3, 4.4 } et b est représenté par le tableau { 2.0, -1.0, 0.0 }, alors
l'appel de ProduitExterne(a, b), retournerait le tableau bidimentionnel:

    4.4     -2.2    0
    6.6     -3.3    0
    8.8     -4.4    0
        
Son élément p[i,j] est le produit de a[i] et b[j].

## b) Tableau en escalier

Écrivez et testez la fonction suivante:

    double[][] Produit(double[] a , double[] b);

La fontion renvoie le produit externe des éléments de a avec les éléments de b. Par exemple,  si a est
représenté par le tableau { 2.2, 3.3, 4.4 } et b est représenté par le tableau { 2.0, -1.0, 0.0 }, alors
l'appel de ProduitExterne(a, b), retournerait le tableau bidimentionnel:

    4.4     -2.2    0
    6.6     -3.3    0
    8.8     -4.4    0
        
Son élément p[i][j] est le produit de a[i] et b[j].

## Exécution des tests

Pour exécuter les tests, exécutez la commande `dotnet test` à partir du répertoire de l'exercice.

Initialement, seul le premier test sera activé. C'est pour vous encourager à résoudre l'exercice une étape à la fois.
Une fois que vous avez réussi le premier test, supprimez la propriété `Skip` du test suivant et travaillez à faire passer ce prochain test.
Une fois qu'aucun des tests n'est ignoré et qu'ils réussissent tous, vous devez soumettre votre solution.
