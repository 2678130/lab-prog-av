using Xunit;

namespace tableau_bidimentionnel
{
    public class TableauEnEscalierTest
    {
        [Theory]
        [InlineData(new double[] { 2.2, 3.3, 4.4 }, new double[] { 2.0, -1.0, 0.0 })]
        public void Test1(double[] a, double[] b)
        {
            var result = TableauEnEscalier.Produit(a, b);

            Assert.Equal(a[0] * b[0], result[0][0]);
            Assert.Equal(a[0] * b[1], result[0][1]);
            Assert.Equal(a[0] * b[2], result[0][2]);

            Assert.Equal(a[1] * b[0], result[1][0]);
            Assert.Equal(a[1] * b[1], result[1][1]);
            Assert.Equal(a[1] * b[2], result[1][2]);

            Assert.Equal(a[2] * b[0], result[2][0]);
            Assert.Equal(a[2] * b[1], result[2][1]);
            Assert.Equal(a[2] * b[2], result[2][2]);
        }
    }
}
