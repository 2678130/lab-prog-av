using System.Linq;

namespace tableau_bidimentionnel
{
    public class TableauRectangle
    {
        // a0b0 a1b0 a2b0,
        // a1b0 a1b1 a1b2 ...
        public static double[,] Produit(double[] a, double[] b)
        {
            var tableauProduit = new double[a.Length, b.Length];
            foreach(var index in Enumerable.Range(0, tableauProduit.Length))
            {
                // current row in foreach
                var row = index / b.Length;

                // current column in foreach
                var col = index % b.Length;

                // value of current square = array [a] row * array b[col]
                tableauProduit[row, col] = a[row] * b[col];
            }
            return tableauProduit;
        }
    }
}
