using System;
using System.Collections.Generic;
using Xunit;

public class NombreDeNucleotidesTest
{
    [Fact]
    public void Brin_vide()
    {
        var expected = new Dictionary<char, int>
        {
            ['A'] = 0,
            ['C'] = 0,
            ['G'] = 0,
            ['T'] = 0
        };
        Assert.Equal(expected, NombreDeNucleotides.Count(""));
    }

    [Fact]
    public void Compte_un_nucleotide_dans_un_brin_a_lettre_unique()
    {
        var expected = new Dictionary<char, int>
        {
            ['A'] = 0,
            ['C'] = 0,
            ['G'] = 1,
            ['T'] = 0
        };
        Assert.Equal(expected, NombreDeNucleotides.Count("G"));
    }

    [Fact]
    public void Brin_avec_nucleotide_repete()
    {
        var expected = new Dictionary<char, int>
        {
            ['A'] = 0,
            ['C'] = 0,
            ['G'] = 7,
            ['T'] = 0
        };
        Assert.Equal(expected, NombreDeNucleotides.Count("GGGGGGG"));
    }

    [Fact]
    public void Brin_avec_plusieurs_nucleotides()
    {
        var expected = new Dictionary<char, int>
        {
            ['A'] = 20,
            ['C'] = 12,
            ['G'] = 17,
            ['T'] = 21
        };
        Assert.Equal(expected, NombreDeNucleotides.Count("AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC"));
    }

    [Fact]
    public void Brin_avec_nucleotides_invalides()
    {
        Assert.Throws<ArgumentException>(() => NombreDeNucleotides.Count("AGXXACT"));
    }
}