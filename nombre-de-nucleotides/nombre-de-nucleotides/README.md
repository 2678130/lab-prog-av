# Nombre de nucléotides

Étant donné un simple brin d'ADN, calculez combien de fois chaque nucléotide apparaît dans ce brin.

L'acide désoxyribonucléique ou ADN contient toute l'information génétique, appelée génome, permettant le développement, le fonctionnement et la reproduction des êtres vivants. L'ADN est une macromolécule biologique construite à partir d'une séquence extrêmement longue d'éléments individuels appelés nucléotides. L'ordre dans lequel se succèdent les nucléotides le long d'un brin d'ADN constitue la séquence de ce brin. Il n'exsite que quatre bases nucléiques dans l'ADN. Celles-ci ne diffèrent que légèrement et peuvent être représentés par les symboles suivants: «A» pour l'adénine, «C» pour la cytosine, «G» pour la guanine et «T» thymine.

## Conseils

Cet exercice nécessite l'utilisation d'un [dictionnaire](https://docs.microsoft.com/fr-ca/dotnet/api/system.collections.generic.idictionary-2?view=netframework-4.8)

## Exécution des tests

Pour exécuter les tests, exécutez la commande `dotnet test` à partir du répertoire de l'exercice.

Initialement, seul le premier test sera activé. C'est pour vous encourager à résoudre l'exercice une étape à la fois.
Une fois que vous avez réussi le premier test, supprimez la propriété `Skip` du test suivant et travaillez à faire passer ce prochain test.
Une fois qu'aucun des tests n'est ignoré et qu'ils réussissent tous, vous devez soumettre votre solution.
