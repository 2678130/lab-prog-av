using System;
using System.Collections.Generic;

public static class NombreDeNucleotides
{
    /// <remarks>
    /// En .NET la méthode Count et la proprieté Length sont utilisées pour
    /// obtenir le nombre d'éléments dans une collection.
    /// </remarks>
    public static IDictionary<char, int> Count(string brin)
    {
        var tableauDuNombreDeNucleotide = new Dictionary<char, int>()
        {
            { 'A', 0},
            { 'C', 0},
            { 'G', 0},
            { 'T', 0}
        };

        // Iterer sur le brin, puis augmenter le valeur (int => compte) a la cle (char => Nucleotide)
        foreach(char Nucleotide in brin)
        {
            // verification si le tableau contient la cle
            if (!tableauDuNombreDeNucleotide.ContainsKey(Nucleotide)) throw new ArgumentException("Nom de nucleotide invalide");

            tableauDuNombreDeNucleotide[Nucleotide] += 1;
        }

        return tableauDuNombreDeNucleotide;
    }
}