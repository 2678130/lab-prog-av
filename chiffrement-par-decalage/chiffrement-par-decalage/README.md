# Chiffrement par décalage

Créez une implémentation du chiffrement par décalage, aussi connu comme le chiffre de César ou le code de César.

[https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage](https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage)

Le texte chiffré s'obtient en remplaçant chaque lettre du texte clair original par une lettre à distance fixe (`int`) entre `0` et `26`, toujours du même côté, dans l'ordre de l'alphabet. Pour les dernières lettres (dans le cas d'un décalage à droite), on reprend au début.

Un tel code avec un décalage de 13 caractères est aussi employé dans l'algorithme ROT13.

```text
ROT + <clé>
```

À l’aide de la définition de cet algorithme (décalage de 13 caractères de chaque lettre), on peut alors définir la correspondance entre les caractères en clair et chiffrés :

```text
Caractère non-chiffré: 	abcdefghijklmnopqrstuvwxyz
Caractère chiffré: 	    nopqrstuvwxyzabcdefghijklm
```

## Exemples

- ROT5  `web`              on obtient `bjh`
- ROT0  `d`                on obtient `d`
- ROT26 `Bonjour le monde` on obtient `Bonjour le monde`
- ROT13 `Chiffre moi.`     on obtient `Puvsser zbv.`
- ROT13 `Puvsser zbv.`     on obtient `Chiffre moi.`

## Exécution des tests

Pour exécuter les tests, exécutez la commande `dotnet test` à partir du répertoire de l'exercice.

Initialement, seul le premier test sera activé. C'est pour vous encourager à résoudre l'exercice une étape à la fois.
Une fois que vous avez réussi le premier test, supprimez la propriété `Skip` du test suivant et travaillez à faire passer ce prochain test.
Une fois qu'aucun des tests n'est ignoré et qu'ils réussissent tous, vous devez soumettre votre solution.
