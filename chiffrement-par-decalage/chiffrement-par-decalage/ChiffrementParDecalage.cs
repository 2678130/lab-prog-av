using System;

public static class ChiffrementParDecalage
{
    public static string Decaler(string texte, int cleDeChiffrement)
    {
        // maj => 65 a 90
        // min => 97 a 122
        // space => 0

        // ideally we would want to go (int)char + offset, however, 
        // if initial is 120 + 5 => 125 => we should get 99,
        // which is also technically 120 + (5 - 26),
        // in the case that sum is above the upperbound we substract from the position

        // How do we handle the min maj space situation?
        // upperbound is either 90 or 122, 
        // if v > 96 => upperbound => 122 else upperbound = 90

        // also need to account for space, I guess if char value < 65 => set char value and continue

        var codedText = new char[texte.Length];

        for (var i = 0; i < texte.Length; i ++)
        {
            int currentCharValue = (int)texte[i];

            // case for space
            if(currentCharValue < 65)
            {
                codedText[i] = texte[i];
                continue;
            }

            // get upper bound
            int upperbound = currentCharValue > 96 ? 122 : 90;

            // set the value correctly
            codedText[i] = (currentCharValue + cleDeChiffrement) > upperbound ? (char)(currentCharValue + (cleDeChiffrement - 26)) : (char)(currentCharValue + cleDeChiffrement);
        }
        return new string(codedText);
    }
}
