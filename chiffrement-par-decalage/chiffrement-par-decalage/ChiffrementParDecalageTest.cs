using Xunit;

public class ChiffrementParDecalageTest
{
    [Fact]
    public void Faire_pivoter_a_de_0_meme_sortie_que_entree()
    {
        Assert.Equal("a", ChiffrementParDecalage.Decaler("a", 0));
    }

    [Fact]
    public void Faire_pivoter_a_de_1()
    {
        Assert.Equal("b", ChiffrementParDecalage.Decaler("a", 1));
    }

    [Fact]
    public void Faire_pivoter_a_de_26_meme_sortie_que_entree()
    {
        Assert.Equal("a", ChiffrementParDecalage.Decaler("a", 26));
    }

    [Fact]
    public void Faire_pivoter_m_de_13()
    {
        Assert.Equal("z", ChiffrementParDecalage.Decaler("m", 13));
    }

    [Fact]
    public void Faire_pivoter_n_de_13_avec_permutation_circulaire()
    {
        Assert.Equal("a", ChiffrementParDecalage.Decaler("n", 13));
    }

    [Fact]
    public void Faire_pivoter_les_majuscules()
    {
        Assert.Equal("FXYJWNC", ChiffrementParDecalage.Decaler("ASTERIX", 5));
    }

    [Fact]
    public void Faire_pivoter_les_espaces()
    {
        Assert.Equal("H Q J T U F Y W J", ChiffrementParDecalage.Decaler("C L E O P A T R E", 5));
    }

    [Fact]
    public void Faire_pivoter_les_nombres()
    {
        Assert.Equal("Fvyxyw 1 2 3 fvyxyw", ChiffrementParDecalage.Decaler("Brutus 1 2 3 brutus", 4));
    }

    [Fact]
    public void Faire_pivoter_les_ponctuations()
    {
        Assert.Equal("ez npdn qzip, e'vd qp, e'vd qvdixp!", ChiffrementParDecalage.Decaler("je suis venu, j'ai vu, j'ai vaincu!", 21));
    }

    [Fact]
    public void Faire_pivoter_toutes_les_lettres()
    {
        // https://fr.wikipedia.org/wiki/The_quick_brown_fox_jumps_over_the_lazy_dog
        Assert.Equal("Gur dhvpx oebja sbk whzcf bire gur ynml qbt.", ChiffrementParDecalage.Decaler("The quick brown fox jumps over the lazy dog.", 13));
    }
}
