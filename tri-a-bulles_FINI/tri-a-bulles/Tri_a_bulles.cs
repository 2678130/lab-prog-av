using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace tri_a_bulles
{
    public class Tri_a_bulles
    {
        public static int[] Trier(int[] arr)
        {
            // TODO: Pseudo-code à completer

            // CHANGEMENT dans le pseudo-code
            // Tri à bulle ...
            // En commencant par le début, on passe tous les éléments
            // La première passe on fait le tableau en entier mais de passe en passe on garanti
            // que le dernier élément est le plus grand donc on ne doit plus vérifier le dernier
            //
            // Pour chaque element de i = array.Lenght, tant que i > 0, i --
            for (var i = arr.Length; i > 0; i--)
            {
                // Pour chaque element de j = 1, tant que j < i, j++
                // Donc on commence par le début et on inverse quand l'élément à l'index j est plus petit que l'élément à l'index j - 1
                for (var j = 1; j < i; j++)
                {
                    // Inverse lorsque l'élément à l'index j est plus petit que l'élément à l'index j-1
                    if(arr[j] < arr[j-1])
                    {
                        Swap(j, j - 1, arr);
                    }
                }
            }
            return arr;
        }

        /// <summary>
        /// Fonction pour inverser deux index, modifie le tableau passer en paramètre
        /// </summary>
        /// <param name="index1">Premier index</param>
        /// <param name="index2">Second index</param>
        /// <param name="arr">Tableau à modifier</param>
        private static void Swap(int index1, int index2, int[] arr)
        {
            var temp = arr[index1];
            arr[index1] = arr[index2];
            arr[index2] = temp;
        }
    }
}
