using System;
using System.Linq;
using Xunit;

namespace tri_a_bulles
{
    public class Tri_a_bullesTest
    {
        [Fact]
        public void Avec_un_tableau_vide_on_retourne_un_tableau_vide()
        {
            var sut = new int[] {};

            var result = Tri_a_bulles.Trier(sut);

            Assert.Equal(0, result.Length);
        }

        [Theory]
        [InlineData(new int[] {-1, 100, 200, 5})]
        [InlineData(new int[] {12, 34, 3, 6666, 77})]
        public void Avec_un_tableau_on_retourne_un_tableau_trie(int[] sut)
        {
            var result = Tri_a_bulles.Trier(sut);

            Assert.Equal(result.Min(), result[0]);                 // Premier élément
            Assert.Equal(result.Skip(1).Min(), result[1]);         // Deuxième élément
            Assert.Equal(result.SkipLast(1).Max(), result[^2]);    // Avant dernier élément, même chose que result[result.Length - 1]
            Assert.Equal(result.Max(), result[^1]);                // Dernier élément, même chose que result[result.Length - 1]
        }

        [Fact]
        public void Avec_un_tableau_random_on_retourne_un_tableau_trie()
        {
            var random = new Random();
            var sut = Enumerable.Range(0, 1000).Select(i => random.Next(0, 100_000)).ToArray();

            var result = Tri_a_bulles.Trier(sut);

            Assert.Equal(result.Min(), result[0]);                 // Premier élément
            Assert.Equal(result.Skip(1).Min(), result[1]);         // Deuxième élément
            Assert.Equal(result.SkipLast(1).Max(), result[^2]);    // Avant dernier élément, même chose que result[result.Length - 1]
            Assert.Equal(result.Max(), result[^1]);                // Dernier élément, même chose que result[result.Length - 1]
        }
    }
}
